/**
 * @file  Viola.h
 * @brief 乱数生成のユーティリティ。
 * 
 * @auther          Asanizi (W.W.C, Inc.)
 * @date            2008-02-07
 * @version         $Id: Viola.h,v 1.0 2008-02-05 12:36:56 Asanizi Exp $
 * 
 * 
 * Copyright (C) 2008 W.W.C, Inc. All right reserved.
 */

/** \mainpage
 * 
 * <pre>
 * boost::random的な乱数生成器と乱数分布器クラス群を提供しています。
 * </pre>
 * 
 * linear_combinationのtypedef:
 * - UNIX_rand : UNIXで使用される rand() アルゴリズムです。
 * - TurboC15_rand : Turbo C で使用される rand() アルゴリズムです。
 * - VisualC_rand : Visual C で使用される rand() アルゴリズムです。
 * - BorlandC_rand : Borland C で使用される rand() アルゴリズムです。
 *
 * その他の乱数生成器のtypedef:
 * - XorShift : XORシフトを使用する乱数生成器
 * - rand48 : 48bitを使う乱数生成器
 * 
 */
#ifndef Viola_h
#define Viola_h


//#include "../Neutrino/Neutrino.h"

#include "uniform.h"
#include "uniforms.h"
#include "uniform_small_int.h"
#include "uniform_real.h"
#include "uniform_triangle.h"

#include "randomable.h"
#include "linear_combination.h"
#include "rand48.h"
#include "XorShift.h"
#include "mt19937ar.h"

#include "VariationGenerator.h"

#include "distribution.h"
#include "distribution_gamma.h"
#include "distribution_geometric.h"
#include "distribution_poisson.h"
#include "distribution_one_clause.h"
#include "distribution_chisq.h"


////////////////////////////////////////////////////////////////////////////////
//
// Generic内のmixin.hが無いときのための回避策です。
//
#ifndef Generic_mixin_h
    #define   uint      unsigned long
    #define   sint        signed long
#endif  


namespace Viola {
    
    
    ////////////////////////////////////////////////////////////////////////////
    // 
    // 乱数生成器。
    // 
    //! UNIXのrand()。
    typedef linear_combination< 
                          long, 
                    1103515245, 
                         12345, 
                             0, 
                             1
            >                           UNIX_rand;
    //! TurboC 1.5のrand()。
    typedef linear_combination<
                          long, 
                      22695477, 
                             1, 
                             0, 
                             1
            >                           TurboC15_rand;
    //! Visual C++のrand()。
    typedef linear_combination< 
                          long, 
                        214013, 
                       2531011, 
                             0, 
                             1
            >                           VisualC_rand;
    //! Borland C++ のrand()。
    typedef linear_combination< 
                          long, 
                      22695477, 
                             1, 
                             1, 
                      22695478 
            >                           BorlandC_rand;
    
    typedef _x_or_shift<
                unsigned long, 
                    123456789, 
                    362436069, 
                    521288629, 
                     88675123, 
                           11, 
                           19, 
                            8
            >                           XorShift;
    
    ///! 48bitを使う乱数生成器。
    typedef _rand48< /* ... */ >        rand48;
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // 
    // 乱数分布器。
    // 
    typedef _uniform_small_int< /* ... */ >       uniform_smallInt;
    typedef _uniform< long >                      non_uniform;
    typedef _uniform_res53<
                          double, 
                9007199254740992, 
                               1, 
                        67108864
            >                           uniform_res53;
    
    
    ////////////////////////////////////////////////////////////////////////////
    // 
    // 乱数精製用の擬似ファンクタです。
    // 
    typedef variation_generator< 
                    mt19937ar<>, 
                     non_uniform 
            >                           MersenneTwister;
    typedef variation_generator< 
                    mt19937ar<>, 
                  uniform_res53, 
                          double 
            >                           Unif_r;
};





#endif  /* Viola_h */