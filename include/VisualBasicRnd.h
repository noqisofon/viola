#ifndef Viola_VisualBasicRnd_h
#define Viola_VisualBasicRnd_h

#include "randomable.h"


namespace Viola {
    
    
    template < class   IntType =      int,
               IntType A       = 16598013, 
               IntType C       = 12820163, 
               IntType F       =        0, 
               IntType S       =   327680, 
               IntType D       = 16777216, 
               IntType G       = 16777215
             >
    class VisualBasicRnd
        : virtual public randomable<IntType, A, C, F, S, D, G>
    {
     public:
        typedef     double    result_type;
        typedef     double    seed_type;
     
     protected:
        IntType x_;
        
     public:
        VisualBasicRnd(seed_type d) : x_(S) {
            set_seed( d );
        }
        
        void  set_seed(seed_type d) {
            unsigned short* us = (unsigned short *)&d;
            x_ = ( (uc[2] ^ uc[3]) << 8 ) | (x_ & 0xff);
        }
        
        
        result_type get_random() {
            x_ = x_ * A + C & G;
            
            return x_ * (1.0 / (result_type)D);
        }
        result_type get_random(float f) {
            unsigned u = *(unsigned *)&f;
            if (u & 2146828287) {
                if (f < 0) { x_ = (u >> 24) + u };
                x_ = x_ * A + C & G;
            }
            return x_ * (1.0 / (result_type)D);
        }
        
        
    };


};





#endif  /* Viola_VisualBasicRnd_h */
