#ifndef Viola_XorShift_h
#define Viola_XorShift_h

#include "randomable.h"


namespace Viola {
    
    
    /** XORシフトを利用して乱数を作成する乱数生成器です。
     * 
     */
    template < class   IntType /*=  unsigned long*/, 
               IntType X       /*=      123456789*/,
               IntType Y       /*=      362436069*/,
               IntType Z       /*=      521288629*/,
               IntType W       /*=       88675123*/,
               IntType D       /*=             11*/,
               IntType G       /*=             19*/,
               IntType J       /*=              8*/
             >
    class _x_or_shift 
        : virtual public randomable<IntType , X , Y , Z , W , D , G>
    {
     protected:
        IntType x_;
        IntType y_;
        IntType z_;
        IntType w_;
     
     public:
        /**
         * 新しく _x_or_shift オブジェクトを作成します。
         */
        _x_or_shift() : x_(X), y_(Y), z_(Z), w_(W) {}
        _x_or_shift(seed_type s) : x_(s), y_(Y), z_(Z), w_(W) {}
     
     public:
        /**
         * 乱数の種を与えて乱数ジェネレータの状態を変更します。
         *      @param s 乱数の種。
         */
        virtual void     set_seed(seed_type s) { x_ = s; }
        
        
        /**
         * 乱数を取得します。
         *      @return 乱数。
         */
        virtual result_type get_random() {
            IntType t;
            t = ( x_ ^ (x_ << D) );
            
            x_ = y_;
            y_ = z_;
            z_ = w_;
            
            return ( w_ = (w_ ^ (w_ >> G) ) ^ ( t ^ (t >> J) ) );
        }
    };


};





#endif  /* Viola_XorShift_h */
