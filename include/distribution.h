#ifndef Viola_distribution_h
#define Viola_distribution_h

#include <math.h>
#include "Viola.h"
#include "uniform.h"
#include "uniforms.h"


namespace Viola {
    
    
    /** ディストリビューションのルートクラスです。
     * <pre>
     * _distributionは乱数分布器用のアダプターです。
     * ですが、一項ファンクタならば、どんなものにでも付けることができます。
     * </pre>
     */
    template < class ValueGenerator,
               class IntType,
               class IntType2 = IntType,
               class IntType3 = IntType
             > 
    class _distribution
    {
     public:
        /**
         * operator()で返される型です。
         */
        typedef     typename IntType2   result_type;
        /**
         * 一項ファンクタで引数に渡される型です。
         */
        typedef     typename IntType3   input_type;
     
     protected:
        /**!< 一項ファンクタです。 */
        ValueGenerator vg_;
     
     public:
        /** 一項ファンクタを受け取って新しく_distributionを作成します。
         *      @param rng 渡す一項ファンクタ。
         */
        _distribution(const ValueGenerator& rng) : vg_(rng) {}
        /** 他の_distributionを受け取って、新しく_distributionを作成します。
         *      @param rng 渡す一項ファンクタ。
         */
        _distribution(const _distribution& other) : vg_(other.vg_) {}
        
     
     public:
        /** 他のディストリビューションを代入します。
         *      @param 代入したい他のディストリビューション。
         *
         *      @return 自分自身。
         */
        const _distribution& operator = (const _distribution& other) {
            vg_ = other.vg_;
            
            return *this;
        }
        
        
        /** 分布させた乱数を返します。
         * <pre>
         * この関数をオーバーライドして新しいディストリビューションを作成してください。
         * </pre>
         *      @param a 一項ファンクタの引数。
         *
         *      @return 分布させた乱数。
         */
        virtual result_type  distribute(const input_type& a) {
            return vg_() * a;
        }
        
        
        /** 分布させた乱数を返します。
         * <pre>
         * 中身はdistribute()を呼び出すだけです。
         * このオペレータをオーバーライドさせないでください。
         * </pre>
         *      @param a 一項ファンクタの引数。
         *
         *      @return 分布させた乱数。
         */
        result_type operator() (const input_type& a) {
            return distribute(a);
        }
        
        
    };



};





#endif  /* Viola_distribution_h */
