#ifndef Viola_distribution_chisq_h
#define Viola_distribution_chisq_h

#include "distribution_one_clause.h"
#include "distribution_gamma.h"





namespace Viola {
    
    /** 自由度νのカイ２乗分布です。
     * <pre>
     * ガンマ分布を使っているので、一項ディストリビューションになります。
     * </pre>
     */
    template < class Distribution = distribution_gamma<>,
               class RealType     = double 
             >
    class distribution_chisq 
        : virtual public _distribution_one_clause< Distribution , RealType >
    {
     public:
        distribution_chisq(const Distribution& dist) 
            : _distribution_one_clause(dist) 
        {
        }
     
     public:
        /** 分布させた乱数を返します。
         *      @param a 一項ファンクタの引数。
         *
         *      @return 分布させた乱数。
         */
        virtual result_type  distribute(const input_type& a) {
            // distribute_にパラメータを容れる当たりが一項ディストリビュート。
            return 2 * distribute_( 0.5 *  a );
        }
    };


};





#endif  /* Viola_distribution_chisq_h */