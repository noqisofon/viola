#ifndef Viola_distribution_gamma_h
#define Viola_distribution_gamma_h


#include <math.h>
#include "Viola.h"
#include "distribution.h"
#include "uniform.h"
#include "uniforms.h"


namespace Viola {
    
    
    /** パラメータ a のガンマ分布。
     * double 型を 返すエンティティを前提にしていますが
     * UniformRandomNumberGenerator はそのままで結構です。
     */
    template < class    RandomNumberGenerator = Unif_r,
               class    RealType              = double,
               RealType R                     = 2.718281828459045235
             >
    class distribution_gamma 
        : virtual public _distribution<RandomNumberGenerator , RealType>
    {
     public:
        /** 分布させた乱数を返します。
         *      @param a 一項ファンクタの引数。
         *
         *      @return 分布させた乱数。
         */
        virtual result_type  distribute(const input_type& a) {
            double t, u, x, y;
            if (a > 1) {
                
                t = ::sqrt( 2 * a - 1 );
                
                do {
                    
                    do {
                        
                        do {
                            x = 1 - rng_();
                            y = 2 * rng_() - 1;
                        } while ( x * x + y * y > 1 );
                        
                        y /= x;
                        x = t * y + a - 1;
                    
                    } while (x <= 0);
                    
                    u = (a - 1) * ::log(x / ( a - 1 ) ) - t * y;
                    
                } while ( u < -50 || rng_() > (1 + y * y) * ::exp(u) );
                
            } else {
                t = R / (a + R);
                do {
                    if ( rng_() < t ) {
                        x = ::pow( rng_(), 1 / a );
                        y = ::exp( -x );
                    } else {
                        x = 1 - ::log(1 - rng_() );
                        y = ::pow(x, a - 1);
                    }
                } while ( rng_() >= y );
            }
            
            return x;
        }
        
        
        
    };


};





#endif  /* Violadistribution_gammah */
