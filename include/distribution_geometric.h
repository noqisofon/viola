#ifndef Viola_distribution_geometric_h
#define Viola_distribution_geometric_h

#include "distribution.h"





namespace Viola {
    
    
    /** 確率Pの幾何学分布です。
     * 
     */
    template < class RealType              = double,
               class RandomNumberGenerator = Unif_r
             >
    class distribution_geometric 
        : virtual public _distribution<RandomNumberGenerator , RealType , int>
    {
     public:
        distribution_geometric(const RandomNumberGenerator& rng) : rng_(rng) {
        }
     
     public:
        /** 分布させた乱数を返します。
         *      @param a 一項ファンクタの引数。
         *
         *      @return 分布させた乱数。
         */
        virtual result_type distribute(const input_type& a) {
            return (int)::ceil( ::log( 1.0 - rng_() ) / ::log(1 - p) );
        }
    };
    
    
};





#endif  /* Viola_distribution_geometric_h */
