#ifndef Viola_distribution_one_clause_h
#define Viola_distribution_one_clause_h

#include "distribution.h"
#include "uniform.h"
#include "uniforms.h"


namespace Viola {
    
    
    /** 一項ディストリビューションの基底クラスです。
     * <pre>
     * コンストラクタでディストリビューションを受け取ります。
     * このディストリビューションは 1 つの引数を持つ擬似ファンクタです。
     * この特徴のために、バリエーションジェネレータを Distribution として
     * 使うことが出来なくなっています。
     * </pre>
     */
    template < class Distribution,
               class IntType  = typename Distribution::result_type,
               class IntType2 = typename Distribution::input_type
             > 
    class _distribution_one_clause
    {
     public:
        typedef     typename IntType    result_type;
        typedef     typename IntType2   input_type;
     
     protected:
        /** ディストリビューションです。
         * 
         */
        Distribution distribute_;
     
     public:
        _distribution_one_clause(const Distribution& dist) 
            : distribute_(dist) 
        {
        }
        template <class Distribution>
        _distribution_one_clause(const _distribution_one_clause<Distribution>& other) 
            : distribute_(other.distribute_) 
        {
        }
     
     public:
        /** 分布させた乱数を返します。
         *      @param a 一項ファンクタの引数。
         *
         *      @return 分布させた乱数。
         */
        virtual result_type distribute(const input_type& a) {
            return distribute_( a );
        }
        
        
        /** 分布させた乱数を返します。
         * 中身はdistribute()を呼び出すだけです。
         * このオペレータをオーバーライドさせないでください。
         *      @param a 一項ファンクタの引数。
         *
         *      @return 分布させた乱数。
         */
        result_type operator() (const input_type& a) {
            return distribute(a);
        }
    };



};





#endif  /* Viola_distribution_one_clause_h */