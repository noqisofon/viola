#ifndef Viola_distribution_poisson_h
#define Viola_distribution_poisson_h

#include "distribution.h"


namespace Viola {
    
    
    /** 平均λのポアソン分布です。
     * 
     */
    template < class RandomNumberGenerator = Unif_r,
               class RealType              = double
             >
    class distribution_poisson 
        : virtual public _distribution<RandomNumberGenerator, RealType, int>
    {
     public:
        distribution_poisson(const RandomNumberGenerator& rng) : rng_(rng) {
        }
     
     public:
        /** 分布させた乱数を返します。
         *      @param a 一項ファンクタの引数。
         *
         *      @return 分布させた乱数。
         */
        virtual result_type distribute(const input_type& a) {
            int k;
            lamda = ::exp(lamda) * rng_();
            
            for (k = 0; lamda > 1; ++ k) { lamda *= rng_(); }
            
            return k;
        }
    };
    
    
};





#endif  /* Viola_distribution_poisson_h */
