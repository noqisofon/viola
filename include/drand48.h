#ifndef Viola_drand48_h
#define Viola_drand48_h

#include "randomable.h"





namespace Viola {

    /* ---------------------------------------------------------------------- *
       ■ drand48
       ======================================================================
       48bitの線形合同法を使用したMath.random()系ファンクタ。
       設計がおかしかったので、意図された値が返ってこないかもしれません。
       素直にrand48にuniform_realを通してください。
     * ---------------------------------------------------------------------- */
    template < class IntType =       long long,
               IntType A     =     0x5deece66d, 
               IntType C     =             0xb, 
               IntType F     =          0x330e, 
               IntType S     =  0x1234abcd330e, 
               IntType D     =      0xffffffff, 
               IntType G     = 281474976710656
             >
    class drand48 : virtual public randomable<IntType,A,C,F,S,D,G>
    {
     protected:
        IntType x_;
        
     public:
        drand48(IntType s = S) {
            set_seed(s);
        }
     
     public:
        void set_seed(IntType s) {
            x_ = s;
            x_ = (x_ << D) + F;
        }
        void  operator() (IntType s) {
            x_ = s;
            x_ = (x_ << D) + F;
        }
        
        
        // 0.0以上1.0未満。
        double get_random() {
            x_ = x_ * A + C;
            
            return (x_ & D ) * (1.0 / (double)G);
        }
        IntType  operator() () {
            x_ = x_ * A + C;
            
            return (x_ & D ) * (1.0 / (double)G);
        }
        
    };


};





#endif  /* Viola_drand48_h */
