#ifndef Viola_linear_combination_h
#define Viola_linear_combination_h

#include "randomable.h"





namespace Viola {

    /**
     * 線形合同法乱数ジェネレータの基底クラスです。
     * <pre>
     * IntType は勿論、 A, C, F, S を決めなければいけないので、
     * Viola.h に記述されたそれぞれの linear_combination エイリアスを使って下さい。
     * </pre>
     */
    template < class   IntType,
               IntType A, 
               IntType C, 
               IntType F, 
               IntType S, 
               IntType D =    16, 
               IntType G = 32767
             >
    class linear_combination 
        : public virtual randomable<IntType,A,C,F,S,D,G>
    {
     protected:
        IntType x_;           /** 乱数の種です。 */
        
     public:
        /**
         * 乱数の種を受け取って新しくlinear_combinationを作成します。
         *      @param s 乱数の種。
         */
        linear_combination(IntType s = S) {
            set_seed(s);
        }
        /**
         * 他のlinear_combinationを受け取って新しくlinear_combinationを作成します。
         *      @param s 他の linear_combination オブジェクト。
         */
        linear_combination(const linear_combination& other) : x_(other.x_) {
        }
     
     public:
        /**
         * linear_combination オブジェクトを代入します。
         */
        const linear_combination& operator = (const linear_combination& other) {
            x_ = other.x_;
            
            return *this;
        }
        
        
        /**
         * 乱数の種を与えて乱数ジェネレータの状態を変更します。
         *      @param s 乱数の種。
         */
        virtual void set_seed(IntType s) {
            x_ = s;
            if (F) get_random();
        }
        
        
        /**
         * 乱数を取得します。
         *      @return 乱数。
         */
        virtual IntType get_random() {
            x_ = x_ * A + C;
            
            return (x_ >> D) & G;
        }
        
    
    
    };


};





#endif  /* Viola_linear_combination_h */
