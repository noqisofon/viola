#ifndef Viola_mrand48_h
#define Viola_mrand48_h

#include "randomable.h"





namespace Viola {
    /* ---------------------------------------------------------------------- *
       ■ _mrand48 < randomable
       ======================================================================
       48bitの線形合同法を使用したMath.random()系ファンクタ。
       符号付きの乱数を返します。
       これも、uniform系を使用すれば可能なので、素直にrand48使ってください。
     * ---------------------------------------------------------------------- */
    template < class   IntType =       long long,
               IntType A       =     0x5deece66d, 
               IntType C       =             0xb, 
               IntType F       =          0x330e, 
               IntType S       =  0x1234abcd330e, 
               IntType D       =              16, 
               IntType G       =      0xffffffff
             >
    class _mrand48 : virtual public randomable<IntType,A,C,F,S,D,G>
    {
     protected:
        IntType x_;
        
     public:
        mrand48(IntType s = S) {
            set_seed(s);
        }
     
     public:
        void set_seed(IntType s) {
            x_ = s;
            x_ = (x_ << D) + F;
        }
        void  operator() (IntType s) {
            x_ = s;
            x_ = (x_ << D) + F;
        }
        
        
        // -2147483648以上2147483647以下。
        IntType get_random() {
            x_ = x_ * A + C;
            
            return (IntType)(x_ >> (D) ) & G;
        }
        IntType  operator() () {
            x_ = x_ * A + C;
            
            return (IntType)(x_ >> (D) ) & G;
        }
        
    };


};





#endif  /* Viola_mrand48_h */
