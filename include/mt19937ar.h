#ifndef Viola_mt19937ar_h
#define Viola_mt19937ar_h


#include "randomable.h"
////////////////////////////////////////////////////////////////////////////////
//
// mt19937ar.cについていたuniformアルゴリズムをクラス化したものです。
//
#include "uniforms.h"


namespace Viola {
    
    
    /** メルセンヌ・ツイスターです。
     * <pre>
     * mt19937arは乱数ジェネレータの一種です。
     * そのままでもお使いいただけますが、variation_generatorに_uniform派生の
     * オブジェクトと一緒に渡すことによっておいしくいただけます。
     * </pre>
     */
    template < class   IntType    = unsigned int,  // パラメータの型。
               IntType N          =          624,  // ベクトルが持つ次元。
               IntType M          =          379,  // 使用用途が不明な値です。
               IntType MATRIX_A   = 0x9908b0dfuL,  // 一定のベクトル。
               IntType UPPER_MASK = 0x80000000uL,  // 大部分の重要なw-rビット。
               IntType LOWER_MASK = 0x7fffffffuL   // 最も少なく重要なrビット。
             >
    class mt19937ar 
        : virtual public randomable<IntType , N , M , MATRIX_A , UPPER_MASK , LOWER_MASK , 0>
    {
     public:
        /**
         * メルセンヌ・ツイスターが持つ配列要素の型です。
         */
        typedef     typename IntType    mt_type;
     
     protected:
        /** 
         * メルセンヌ・ツイスターが持つ次元です。
         */
        mt_type     mt_[N];
        /**
         * 配列のインデックスです。
         */
        int         index_;
     
     protected:
        /**
         * メルセンヌ・ツイスターを初期化します。
         *      @param s 乱数の種。
         */
        void init_genrand(IntType s) {
            mt_[0] = s & 0xffffffffuL;
            
            for (index_ = 1; index_ < N; ++ index_) {
                mt_[index_] = 
                    (1812433253uL * (mt_[index_ - 1] ^ (mt_[index_ - 1] >> 30) ) + index_);
                mt_[index_] &= 0xffffffffuL;
                /* &gt; 32 bitマシン用。 */
            }
        }
        
        
        /**
         * 配列を受け取ってメルセンヌ・ツイスターを初期化します。
         *      @param   init_key 渡す配列。
         *      @param key_length 配列の長さ。
         */
        void init_by_array(IntType* init_key/*[]*/, int key_length) {
            int i, j, k;
            init_genrand( 19650218uL );
            
            i = 1; 
            j = 0;
            k = (N > key_length ? N : key_length);
            
            for (; k; k--) {
                mt_[i] = (mt_[i] ^ ( (mt_[i-1] ^ (mt_[i-1] >> 30) ) * 1664525uL) )
                    + init_key[j] + j; /* non linear */
                mt_[i] &= 0xffffffffuL; /* for WORDSIZE > 32 machines */
                i ++; 
                j ++;
                if (i >= N) { 
                    mt_[0] = mt_[N - 1]; 
                    i = 1; 
                }
                if (j >= key_length) { j = 0; }
            }
            
            for (k = N - 1; k; k --) {
                /* non linear */
                mt_[i] = (mt_[i] ^ ( (mt_[i-1] ^ (mt_[i-1] >> 30) ) * 1566083941uL) ) - i; 
                mt_[i] &= 0xffffffffuL; /* for WORDSIZE > 32 machines */
                i++;
                if (i >= N) { mt_[0] = mt_[N-1]; i=1; }
            }

            mt_[0] = 0x80000000uL; /* MSB is 1; assuring non-zero initial array */ 
        }
    
    public:
        /**
         * 乱数の種を受け取って、新しくmt19937arオブジェクトを作成します。
         */
        mt19937ar(IntType s = 19650218uL) : index_(N + 1) { init_genrand(s); }
        /**
         * 配列を受け取って、新しくmt19937arオブジェクトを作成します。
         */
        mt19937ar(IntType* init_key/*[]*/, int key_length) : index_(N + 1) {
             
            init_by_array(init_key, key_length); 
        }
        /**
         * 他のmt19937arオブジェクトを受け取って、新しくmt19937arオブジェクトを作成します。
         */
        mt19937ar(const mt19937ar& other) : index_(other.index_) {
            for (int i = 0; i < N; ++ i) {
                mt_[i] = other.mt_[i];
            }
        }
        
        
        /**
         * mt19937arオブジェクトを破棄します。
         */
        virtual ~mt19937ar() {}
    
    public:
        /**
         * 乱数の種を与えて乱数ジェネレータの状態を変更します。
         *      @param s 乱数の種。
         */
        virtual void set_seed(seed_type s) { 
            init_genrand(s); 
        }
        
        
        /**
         * 32bit整数型の乱数を計算して返します。
         *      @return 32bit 整数型の乱数。
         */
        //IntType genrand_int32() {
        result_type get_random() {
            result_type y;
            static result_type mag01[2] = {0x0uL, MATRIX_A};
            /* mag01[x] = x * MATRIX_A  for x=0,1 */

            if (index_ >= N) { /* generate N words at one time */
                int kk;

                if (index_ == N+1)   /* if init_genrand() has not been called, */
                    init_genrand(5489uL); /* a default initial seed is used */

                for (kk = 0; kk < N - M; kk ++) {
                    y       = (mt_[kk] & UPPER_MASK) | (mt_[kk + 1] & LOWER_MASK);
                    mt_[kk] = mt_[kk+M] ^ (y >> 1) ^ mag01[y & 0x1uL];
                }
                for (; kk < N - 1; kk ++) {
                    y       = (mt_[kk] & UPPER_MASK) | (mt_[kk + 1] & LOWER_MASK);
                    mt_[kk] = mt_[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1uL];
                }
                y = (mt_[N - 1] & UPPER_MASK) | (mt_[0] & LOWER_MASK);
                mt_[N - 1] = mt_[M - 1] ^ (y >> 1) ^ mag01[y & 0x1uL];

                index_ = 0;
            }
          
            y = mt_[index_ ++];

            /* Tempering */
            y ^= (y >> 11);
            y ^= (y <<  7) & 0x9d2c5680uL;
            y ^= (y << 15) & 0xefc60000uL;
            y ^= (y >> 18);

            return y;
        }
    };



};





#endif  /* mt19937ar_h */
