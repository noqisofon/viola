#ifndef Viola_rand48_h
#define Viola_rand48_h

#include "randomable.h"


namespace Viola {
    
    
    /** 48bitの線形合同法を使用した乱数ジェネレータです。
     * デフォルトでは、符号無し 64 bit 乱数を返します。
     */
    template < class   IntType =  unsigned long long,
               IntType A       =         0x5deece66d, 
               IntType C       =                 0xb, 
               IntType F       =              0x330e, 
               IntType S       =      0x1234abcd330e, 
               IntType D       =                  16, 
               IntType G       =          0x7fffffff
             >
    class _rand48 : virtual public randomable<IntType,A,C,F,S,D,G>
    {
     protected:
        /**
         * 乱数の種です。 
         **/
        IntType     x_;  
        
     public:
        /**
         * 乱数の種を受け取って新しく_rand48を作成します。
         *      @param s 乱数の種。
         */
        _rand48(IntType s = S) {
            set_seed(s);
        }
        /**
         * 他の_rand48を受け取って新しく_rand48を作成します。
         *      @param other 他の_rand48オブジェクト。
         */
        _rand48(const _rand48& other) : _x(other.x_) {}
     
     public:
        /**
         * 乱数の種を与えて乱数ジェネレータの状態を変更します。
         *      @param s 乱数の種。
         */
        void set_seed(IntType s) {
            x_ = s;
            x_ = (x_ << D) + F;
        }
        
        
        /**
         * 乱数を取得します。
         *      @return 0 以上 2147483647 以下の乱数。
         */
        result_type get_random() {
            x_ = x_ * A + C;
            
            return (x_ >> (D + 1) ) & G;
        }
        
        
    };



};





#endif  /* Viola_rand48_h */
