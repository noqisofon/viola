#ifndef Viola_randomable_h
#define Viola_randomable_h

#include "Viola.h"


namespace Viola {
    
    
    /**
     * 乱数ジェネレータの抽象基底クラスです。
     * <pre>
     * 乱数は、self()で取得することができます。
     * </pre>
     */
    template < class   IntType,    // 下記のパラメータの型。
               IntType A,          // 成長値。
               IntType C,          // 成長補正値。
               IntType F,          // set_seed()の中でget_ramdom()するフラグ。
               IntType S,          // 種の初期値。
               IntType D,          // 長さ調整。
               IntType G           // 地面の高さ。
             >
    class randomable
    {
     public:
        /**
         * 返される乱数の型です。
         */
        typedef     typename IntType    result_type;
        /**
         * 乱数の種の型です。
         */
        typedef     typename IntType    seed_type;
     
     public:
        enum Parameter {
            Growth             = A,  //!< 成長値です。
            Growth_Revision    = C,  //!< 成長補正値です。
            RandamizeFlag      = F,  //!< set_seed()の中でget_random()するかどうかのフラグです。
            Initial_Value      = S,  //!< 種の初期値です。
            Length_Coefficient = D,  //!< 長さ調整の値です。
            Ground_Height      = G   //!< 地面の高さです。
        };
     
     public:
        /**
         * 新しくrandomableオブジェクトを作成します。
         */
        randomable() {}
        /**
         * randomableオブジェクトを破棄します。
         */
        virtual ~randomable() {}
     
     public:
        /**
         * randomableオブジェクトを代入します。
         */
        const randomable& operator = (const randomable& other) {
            return *this;
        }
     
     public:
        /**
         * 乱数の種を与えて乱数ジェネレータの状態を変更します。
         *      @param s 乱数の種。
         */
        virtual void     set_seed(seed_type s) = 0;
        /**
         * 乱数の種を与えて乱数ジェネレータの状態を変更します。
         *      @param s 乱数の種。
         */
        virtual void  operator() (seed_type s) { set_seed(s); }
        
        
        /**
         * 乱数を取得します。
         *      @return 乱数。
         */
        virtual result_type  get_random()  = 0;
        /**
         * 乱数を取得します。
         *      @return 乱数。
         */
        virtual result_type  operator() () { return get_random(); }
    };


};





#endif  /* Viola_randomable_h */
