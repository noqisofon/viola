#ifndef Viola_uniform_h
#define Viola_uniform_h


namespace Viola {
    
    
    /** 乱数分布器の仮想基底クラスです。
     * <pre>
     * 乱数生成器は整数(主に32bit)の乱数しか返さないので、この分布器で
     * 実数にしたりする必要があります。
     * </pre>
     */
    template <class IntType> class _uniform
    {
     public:
        /**
         * 返される乱数の型です。
         */
        typedef     typename IntType    result_type;
     
     public:
        /** 乱数ジェネレータを受け取って、乱数を分布させます。
         * @param 渡す乱数ジェネレータ。
         *
         * @return 分布させた乱数。
         */
        template <class UniformRandomNumberGenerator>
        result_type operator() (UniformRandomNumberGenerator& urng) {
            return get_uniform(urng);
        }
        
        
        /** 乱数ジェネレータを受け取って、乱数を分布させます。
         * @param 渡す乱数ジェネレータ。
         *
         * @return 分布させた乱数。
         */
        template <class UniformRandomNumberGenerator>
        result_type get_uniform(UniformRandomNumberGenerator& rng) {
            return rng();
        }
    };
    
    
};


#endif  /* Viola_uniform_h */