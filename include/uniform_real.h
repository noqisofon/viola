/*! @file
 @brief 乱数を実数の0から1まで分布させるクラスです。
 
 このクラスは_uniformから継承しています。
 
 */
#ifndef Viola_uniform_real_h
#define Viola_uniform_real_h

#include <math.h>
#include "uniform.h"


namespace Viola {
    
    
    /** 0.0から1.0までの実数を分布させます。
     * 
     */
    template 
    < 
      class    RealType =      double,
      RealType BITRANGE =          32
    > 
    class _uniform_real : virtual public _uniform<RealType>
    {
      public:
        typedef     RealType   input_type;         //! 
     
     protected:
        RealType    coffee_;  //! 乱数の係数です。
        RealType    max_;     //! 分布の最大値です。
        RealType    min_;     //! 分布の最小値です。
     
     public:
        /** 最小値と最大値を渡して新しくuniform_realを作成します。
         *      @param n 最小値。
         *      @param m 最大値。
         */
        _uniform_real(input_type n = 0.0, input_type m = 1.0) 
            : max_( max(n, m) ), min_( min(n, m) ), coffee_( 1.0 ) 
        {
            coffee_ = 1.0 / pow( 2 , BITRANGE );
        }
        
     public:
        /** 乱数ジェネレータを受け取って、乱数を分布させます。
         *      @param 渡す乱数ジェネレータ。
         *
         *      @return 分布させた乱数。
         */
        template <class UniformRandomNumberGenerator>
        result_type get_uniform(UniformRandomNumberGenerator& rng) {
            return ( ( (result_type)urng() * coffee_ ) * max_ ) + min_;
        }
        
        
    };


};





#endif  /* Viola_uniform_real_h */