#ifndef Viola_uniform_small_int_h
#define Viola_uniform_small_int_h

#include "uniform.h"





namespace Viola {


    /** 
     * 
     */
    template <class IntType = int>
    class _uniform_small_int : virtual public _uniform<IntType>
    {
     public:
        typedef     IntType   input_type;         //! 入力用の型です。
     
     protected:
        IntType     min_;     //! 分布の最小値です。
        IntType     max_;     //! 分布の最大値です。
     
     public:
        static const bool has_fixed_range = false;  //! 固定範囲を持っているかどうかです。
     
     public:
        /** 最小値と最大値を受け取って_uniform_small_intを作成します。
         *      @param min 分布の最小値。
         *      @param max 分布の最高値。
         */
        _uniform_small_int(input_type min, input_type max) 
            : min_( min ), max_( max )
        {
            min_ = (min > max)? max: min;
            max_ = (min > max)? min: max;
        }
        /** 他の_uniform_small_intを受け取って、新しい_uniform_small_intを作成します。
         * 
         */
        _uniform_small_int(const _uniform_small_int& other) 
            : min_( other.min_ ), max_( other.max_ )
        {
        }
     
     public:
        /** 現在の分布の最小値を返します。
         *      @return 分布の最小値。
         */
        result_type get_min() const { return min_; }
        
        
        /** 現在の分布の最大値を返します。
         *      @return 分布の最小値。
         */
        result_type get_max() const { return max_; }
        
        
        /** 分布のそれぞれの値を 0 にします。
         * 
         */
        void reset() { 
            min_ = 0;
            max_ = 0;
        }
     
     
        /** 乱数ジェネレータを受け取って、乱数を分布させます。
         * @param 渡す乱数ジェネレータ。
         *
         * @return 分布させた乱数。
         */
        template <class UniformRandomNumberGenerator>
        result_type get_uniform(UniformRandomNumberGenerator& rng) {
            return ( urng() % (max_ + min_) ) + min_ ;
        }
     
     
    };


};





#endif  /* Viola_uniform_small_int_h */
