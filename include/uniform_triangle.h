#ifndef Viola_uniform_triangle_h
#define Viola_uniform_triangle_h

#include "Viola.h"
#include "uniform.h"
#include "uniforms.h"


namespace Viola {


    /** 三角分布です。
     *
     */
    template < class RealType = double > 
    class _uniform_triangle 
        : virtual public _uniform<RealType>
    {
     public:
        /** 新しく_uniform_triangleを作成します。
         *
         */
        _uniform_triangle() {}
     
     public:
        /** 乱数ジェネレータを受け取って、乱数を分布させます。
         * @param 渡す乱数ジェネレータ。
         *
         * @return 分布させた乱数。
         */
        template <class UniformRandomNumberGenerator>
        result_type get_uniform(UniformRandomNumberGenerator& rng) {
            RealType a = urng(),
                     b = urng() ;
            return a - b;
        }
    };


};





#endif  /* Viola_uniform_triangle_h */
