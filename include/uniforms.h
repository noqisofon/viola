/*! @file
 @brief mt199937ar.cからできたユニフォームクラス群です。
 <pre>
 mt199937arクラスで使用することを想定されたユニフォームクラス群ですが、
 
 </pre>
 */
#ifndef Viola_uniforms_h
#define Viola_uniforms_h

#include "uniform.h"


namespace Viola {
    
    
    template <class IntType> class _uniform;
    
    
    /**
     * 符号付き変数用に分布させます。
     */
    template < class IntType = long long> 
    class _uniform_slong31 : 
        virtual public _uniform<IntType>
    {
     public:
        /**
         * 乱数ジェネレータを受け取って、乱数を分布させます。
         */
        template <class UniformRandomNumberGenerator>
        result_type get_uniform(UniformRandomNumberGenerator& rng) {
            return (result_type)( urng() >> 1 );
        }
    };
    
    
    /**
     * BITMINからBITMAXまでの実数(real)に分布させます。
     */
    template < class     RealType, 
               long long BITMAX,
               long long BITMIN = 1
             >
    class _uniform_fract : public virtual _uniform<RealType>
    {
     public:
        /**
         * 乱数ジェネレータを受け取って、乱数を分布させます。
         */
        template <class UniformRandomNumberGenerator>
        result_type get_uniform(UniformRandomNumberGenerator& rng) {
            return urng() * ( (RealType)BITMIN / (RealType)BITMAX);
        }
    };
    
    
    /**
     * BITMINからCENTERまでの実数(real)に分布させます。
     */
    template < class     RealType =     double, 
               long long BITMAX   = 4294967296, 
               long long BITMIN   =          1,
               long long CENTER   =          2   // 真ん中(1 / 2)。
             >
    class _uniform_real3 : public virtual _uniform<RealType>
    {
     public:
        /**
         * 乱数ジェネレータを受け取って、乱数を分布させます。
         */
        template <class UniformRandomNumberGenerator>
        result_type get_uniform(UniformRandomNumberGenerator& rng) {
            return ( ( (result_type)urng() ) * (1.0 / (RealType)CENTER) ) * ( (RealType)BITMIN / (RealType)BITMAX);
        }
    };
    
    
    
    /**
     * 0.0から1.0までの実数(real)を53bit精度で分布させます。
     */
    template < class     RealType, 
               long long BITMAX, 
               long long BITMIN,
               long long COEFF
             >
    class _uniform_res53 : public virtual _uniform<RealType>
    {
     public:
        /**
         * 乱数ジェネレータを受け取って、乱数を分布させます。
         */
        template <class UniformRandomNumberGenerator>
        result_type get_uniform(UniformRandomNumberGenerator& rng) {
            uint a = urng() >> 5, 
                 b = urng() >> 6 ; 
            return (a * (RealType)COEFF + b) * ( (RealType)BITMIN / (RealType)BITMAX); 
        }


    };
    
    

};





#endif  /* Viola_uniforms_h */
