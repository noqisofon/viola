#ifndef Viola_variation_generator_h
#define Viola_variation_generator_h


#include "Viola.h"




namespace Viola {
    
    /** 乱数生成器と乱数分布器を組み合わせてお好みの分布で、
     *  しかも、お好みの生成アルゴリズムで乱数を生成できるクラスです。
     * 
     */
    template < class Generation,        // 乱数生成器。
               class Distribution,      // 乱数分布器。
               class IntType = long
             >
    class variation_generator
    {
     public:
        /**
         * 返される乱数の値です。
         */
        typedef     typename Distribution::result_type        result_type;
     
     protected:
        Generation            generate_;          //! 乱数生成器です。
        Distribution          distribute_;        //! 乱数分布器です。
     
     public:
        /** 乱数生成器と乱数分布器を受け取って、新しくvariation_generatorを作成します。
         * @param g 渡し乱数生成器。
         * @param d 渡す乱数分布器。
         */
        variation_generator(const Generation& g, const Distribution& d ) 
            : generate_(g), distribute_(d)
        {
        }
        
        
        /**
         * このvariation_generatorを破棄します。
         */
        virtual ~variation_generator() {}
        
     public:
        /** 乱数を分布させた結果を返します。
         * @return 乱数を分布させた結果。
         */
        result_type operator() () { return distribute_( generate_ ); }
    };
    
    
    
};





#endif  /* Viola_variation_generator_h */
