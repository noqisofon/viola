#include "mt19937ar.h"
using namespace Viola;

////////////////////////////////////////////////////////////////////////////////
// 
// ものっそいめんどくさいので、マクロにしてみる。
// 
#define   MT_TEMPLATE         template < class IntType, IntType N, IntType M, \
                                         IntType MATRIX_A, IntType UPPER_MASK, \
                                         IntType LOWER_MASK >



MT_TEMPLATE void mt19937ar<>::init_genrand(IntType s)
{
    mt_[0] = s & 0xffffffffuL;
    
    for (index_ = 1; index_ < N; ++ index_) {
        mt_[index_] = 
            (1812433253uL * (mt_[index_ - 1] ^ (mt_[index_ - 1] >> 30) ) + index_);
        mt[index_] &= 0xffffffffuL;
        /* &gt; 32 bitマシン用。 */
    }
}


MT_TEMPLATE void mt19937ar<>::init_by_array(IntType init_key[], int key_length)
{
    int i, j, k;
    init_genrand( 19650218uL );
    
    i = 1; 
    j = 0;
    k = (N > key_length ? N : key_length);
    
    for (; k; k--) {
        mt_[i] = (mt_[i] ^ ( (mt_[i-1] ^ (mt_[i-1] >> 30) ) * 1664525uL) )
            + init_key[j] + j; /* non linear */
        mt_[i] &= 0xffffffffuL; /* for WORDSIZE > 32 machines */
        i ++; 
        j ++;
        if (i >= N) { 
            mt_[0] = mt_[N - 1]; 
            i = 1; 
        }
        if (j >= key_length) { j = 0; }
    }
    
    for (k = N - 1; k; k --) {
        /* non linear */
        mt_[i] = (mt_[i] ^ ( (mt_[i-1] ^ (mt_[i-1] >> 30) ) * 1566083941uL) ) - i; 
        mt_[i] &= 0xffffffffuL; /* for WORDSIZE > 32 machines */
        i++;
        if (i >= N) { mt_[0] = mt_[N-1]; i=1; }
    }

    mt_[0] = 0x80000000uL; /* MSB is 1; assuring non-zero initial array */ 
}


MT_TEMPLATE IntType mt19937ar<>::genrand_int32(void)
{
    IntType y;
    static IntType mag01[2] = {0x0uL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (index_ >= N) { /* generate N words at one time */
        int kk;

        if (index_ == N+1)   /* if init_genrand() has not been called, */
            init_genrand(5489uL); /* a default initial seed is used */

        for (kk = 0; kk < N - M; kk ++) {
            y       = (mt_[kk] & UPPER_MASK) | (mt_[kk + 1] & LOWER_MASK);
            mt_[kk] = mt_[kk+M] ^ (y >> 1) ^ mag01[y & 0x1uL];
        }
        for (; kk < N - 1; kk ++) {
            y       = (mt_[kk] & UPPER_MASK) | (mt_[kk + 1] & LOWER_MASK);
            mt_[kk] = mt_[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1uL];
        }
        y = (mt_[N - 1] & UPPER_MASK) | (mt_[0] & LOWER_MASK);
        mt_[N - 1] = mt_[M - 1] ^ (y >> 1) ^ mag01[y & 0x1uL];

        index_ = 0;
    }
  
    y = mt_[index_ ++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y <<  7) & 0x9d2c5680uL;
    y ^= (y << 15) & 0xefc60000uL;
    y ^= (y >> 18);

    return y;
}


MT_TEMPLATE sint mt19937ar<>::genrand_int31()
{
    return (long)( genrand_int32() >> 1 );
}


MT_TEMPLATE double mt19937ar<>::genrand_real1()
{
    return genrand_int32() * (1.0 / 4294967295.0);
    // 1 / 2^32-1をかける。
}


MT_TEMPLATE double mt19937ar<>::genrand_real2()
{
    return genrand_int32() * (1.0 / 4294967296.0);
    // 1 / 2^32をかける。
}


MT_TEMPLATE double mt19937ar<>::genrand_real3()
{
    return ( ( (double)genrand_int32() ) + 0.5 ) * (1.0 / 4294967296.0); 
}


MT_TEMPLATE double mt19937ar<>::genrand_res53(void) 
{ 
    unsigned long a = genrand_int32() >> 5, 
                  b = genrand_int32() >> 6 ; 
    return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0); 
} 